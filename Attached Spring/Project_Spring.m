function varargout = Project_Spring(varargin)
% PROJECT_SPRING M-file for Project_Spring.fig
%      PROJECT_SPRING, by itself, creates a new PROJECT_SPRING or raises the existing
%      singleton*.
%
%      H = PROJECT_SPRING returns the handle to a new PROJECT_SPRING or the handle to
%      the existing singleton*.
%
%      PROJECT_SPRING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROJECT_SPRING.M with the given input arguments.
%
%      PROJECT_SPRING('Property','Value',...) creates a new PROJECT_SPRING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Project_Spring_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Project_Spring_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help Project_Spring

% Last Modified by GUIDE v2.5 20-Sep-2006 11:14:53

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Project_Spring_OpeningFcn, ...
                   'gui_OutputFcn',  @Project_Spring_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Project_Spring is made visible.
function Project_Spring_OpeningFcn(hObject, eventdata, handles, varargin)
clc;
disp('A Two Degree of Freedom Mass - Sprong - Damper System')
disp('Ali Soltani    (soltani.110@gmail.com)')
disp('Summer 2006')
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Project_Spring (see VARARGIN)
set(handles.editm1,'String',40);
set(handles.editm2,'String',50);
set(handles.editc1,'String',0);
set(handles.editc2,'String',0);
set(handles.editk1,'String',3);
set(handles.editk2,'String',5);
set(handles.editf1,'String',0);
set(handles.editf2,'String',0);
set(handles.editx0,'String',-2);
set(handles.edity0,'String',0);
set(handles.editvx,'String',0);
set(handles.editvy,'String',1);
set(handles.axes1,'YTick',[]);
set(handles.axes1,'Color',[0.831 0.816 0.784]);
set(handles.axes1,'YColor',[0.831 0.816 0.784]);
axes(handles.axes1)
hold on
plot(handles.axes1,[-1 1],[-.5 -.5],'k-','linewidth',46);
plot(handles.axes1,[0 0],[-1 3],'k-','linewidth',3);
set(handles.axes2,'fontname','time','fontsize',7)
set(handles.axes3,'fontname','time','fontsize',7)
handles.at='1e-3';
handles.rt='1e-6';
handles.L1='1';
handles.L2='1';
handles.tspan=(['[0 10]']);
% Choose default command line output for Project_Spring

handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Project_Spring wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Project_Spring_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editm1_Callback(hObject, eventdata, handles)
% hObject    handle to editm1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editm1 as text
%        str2double(get(hObject,'String')) returns contents of editm1 as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function editm1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editm1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editm2_Callback(hObject, eventdata, handles)
% hObject    handle to editm2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editm2 as text
%        str2double(get(hObject,'String')) returns contents of editm2 as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function editm2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editm2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editk1_Callback(hObject, eventdata, handles)
% hObject    handle to editk1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editk1 as text
%        str2double(get(hObject,'String')) returns contents of editk1 as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function editk1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editk1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editk2_Callback(hObject, eventdata, handles)
% hObject    handle to editk2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editk2 as text
%        str2double(get(hObject,'String')) returns contents of editk2 as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function editk2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editk2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editc1_Callback(hObject, eventdata, handles)
% hObject    handle to editc1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editc1 as text
%        str2double(get(hObject,'String')) returns contents of editc1 as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end
% --- Executes during object creation, after setting all properties.
function editc1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editc1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editc2_Callback(hObject, eventdata, handles)
% hObject    handle to editc2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editc2 as text
%        str2double(get(hObject,'String')) returns contents of editc2 as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function editc2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editc2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editf2_Callback(hObject, eventdata, handles)
% hObject    handle to editf2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editf2 as text
%        str2double(get(hObject,'String')) returns contents of editf2 as a double


% --- Executes during object creation, after setting all properties.
function editf2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editf2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editf1_Callback(hObject, eventdata, handles)
% hObject    handle to editf1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editf1 as text
%        str2double(get(hObject,'String')) returns contents of editf1 as a double


% --- Executes during object creation, after setting all properties.
function editf1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editf1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editx0_Callback(hObject, eventdata, handles)
% hObject    handle to editx0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editx0 as text
%        str2double(get(hObject,'String')) returns contents of editx0 as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function editx0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editx0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function edity0_Callback(hObject, eventdata, handles)
% hObject    handle to edity0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edity0 as text
%        str2double(get(hObject,'String')) returns contents of edity0 as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function edity0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edity0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editvx_Callback(hObject, eventdata, handles)
% hObject    handle to editvx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editvx as text
%        str2double(get(hObject,'String')) returns contents of editvx as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function editvx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editvx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editvy_Callback(hObject, eventdata, handles)
% hObject    handle to editvy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editvy as text
%        str2double(get(hObject,'String')) returns contents of editvy as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
end

% --- Executes during object creation, after setting all properties.
function editvy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editvy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on button press in checkhold.
function checkhold_Callback(hObject, eventdata, handles)
% hObject    handle to checkhold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkhold
if  get(hObject,'Value')    
    set(handles.menu_hold,'Checked','on')
else
    set(handles.menu_hold,'Checked','off')
end

% --- Executes on button press in checkgrid.
function checkgrid_Callback(hObject, eventdata, handles)
% hObject    handle to checkgrid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkgrid
if  get(hObject,'Value')    
    set(handles.axes1,'XGrid','on')
    set(handles.axes1,'YGrid','on')
    set(handles.axes2,'XGrid','on')
    set(handles.axes2,'YGrid','on')
    set(handles.axes3,'XGrid','on')
    set(handles.axes3,'YGrid','on')
    set(handles.menu_grid,'Checked','on')
else
    set(handles.axes1,'XGrid','off')
    set(handles.axes1,'YGrid','off')
    set(handles.axes2,'XGrid','off')
    set(handles.axes2,'YGrid','off')
    set(handles.axes3,'XGrid','off')
    set(handles.axes3,'YGrid','off')
    set(handles.menu_grid,'Checked','off')
end

% --- Executes on button press in draw.
function draw_Callback(hObject, eventdata, handles)
% hObject    handle to draw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% ---------------------------------------------------------------------
x0=str2double(get(handles.editx0,'String'));
y0=str2double(get(handles.edity0,'String'));
vx=str2double(get(handles.editvx,'String'));
vy=str2double(get(handles.editvy,'String'));
m1=str2double(get(handles.editm1,'String'));
m2=str2double(get(handles.editm2,'String'));
c1=str2double(get(handles.editc1,'String'));
c2=str2double(get(handles.editc2,'String'));
k1=str2double(get(handles.editk1,'String'));
k2=str2double(get(handles.editk2,'String'));
f1=get(handles.editf1,'String');
f2=get(handles.editf2,'String');

% --------------------------------------------------------------------
% MS = MarkerSize
ms1=abs(m1);ms2=abs(m2);
ms=max(ms1,ms2);
n=log10(ms);
n=floor(n)+1;
ms1=100*(ms1/10^n)+3;
ms2=100*(ms2/10^n)+3;
minms=min(ms1,ms2);
maxms=max(ms1,ms2);
% --------------------------------------------------------------------
if ~(k1==0&k2==0)
ks1=k1;ks2=k2;
ks=max(ks1,ks2);
n=log10(ks);
n=floor(n)+1;
ks1=floor(40*(ks1/10^n))+10;
ks2=floor(40*(ks2/10^n))+10;
elseif k1==0
    ks1=0;
    if k2==0
        ks2=0;
    end
elseif k2==0
    ks2=0;
end

% --------------------------------------------------------------------
if m1<=0|m2<=0 
    errordlg('??? m <= 0 ','Input Error')
    
    uiwait;
end
% ---------------------------------------------------------------------
RT=str2num(handles.rt);
AT=str2num(handles.at);
L1=str2num(handles.L1);
L2=str2num(handles.L2);
tspan=str2num(handles.tspan);
T=max(tspan);
T0=min(tspan);
% --------------------------------------------------------------------
options=odeset('reltol',RT,'abstol',AT,'NormControl','off');
fun=@(t,y)[y(2)
           1/m1*(eval(f1)-y(2)*(c1+c2)+y(4)*c2-y(1)*(k1+k2)+y(3)*(k2))
           y(4)
           1/m2*(eval(f2)-c2*(y(4)-y(2))-k2*(y(3)-y(1)))];
% ---------------------------------------------------------------------
[t,y]=ode45(fun,tspan,[x0 vx y0 vy],options);
% ---------------------------------------------------------------------
xt=y(:,1);
yt=y(:,3);
vxt=y(:,2);
vyt=y(:,4);
axt=1/m1*(eval(f1)-vxt*(c1+c2)+vyt*c2-xt*(k1+k2)+yt*(k2));
ayt=1/m2*(eval(f2)-c2*(vyt-vxt)-k2*(yt-xt));
xt=y(:,1)+L1;
yt=y(:,3)+(L1+L2);
handles.xt=xt-L1;
handles.vxt=vxt;
handles.axt=axt;
handles.yt=yt-L2-L1;
handles.vyt=vyt;
handles.ayt=ayt;
handles.t=t;
% ---------------------------------------------------------------------
xmax=max(max(xt));
xmin=min(min(xt));
ymax=max(max(yt));
ymin=min(min(yt));
mmax=max(xmax,ymax);
mmin=min(xmin,ymin);
% ---------------------------------------------------------------------
%%%<<<<< Seting Axes 1 >>>>%%%%
axes(handles.axes1);
axis([mmin-3 mmax+3 -1 2]);
hold on
plot([mmin mmax],zeros(1,2),'-');
plot([0 0],[0 3],'-');
%%%<<<<< Seting Axes 2 >>>>%%%%
axes(handles.axes2);
axes2limy=get(handles.axes2,'ylim');
axes2limx=get(handles.axes2,'xlim');
if ~strcmp(get(handles.menu_hold,'Checked'),'on')
    axis([T0 T xmin-L1-.2 xmax-L1+.2]);
else
    ylim([min(axes2limy(1),xmin-L1-.2),max(axes2limy(2),xmax-L1+.2)]);
    xlim([min(axes2limx(1),T0),max(axes2limx(2),T)]);
end

hold on
%%%<<<<< Seting Axes 3 >>>>%%%%
axes(handles.axes3);

axes3limy=get(handles.axes3,'ylim');
axes3limx=get(handles.axes3,'xlim');
if ~strcmp(get(handles.menu_hold,'Checked'),'on')
    axis([T0 T ymin-L1-L2-.2 ymax-L1-L2+.2]);
else
    ylim([min(axes3limy(1),ymin-L1-L2-.2),max(axes3limy(2),ymax-L1-L2+.2)]);
    xlim([min(axes3limx(1),T0),max(axes3limx(2),T)]);
end
hold on
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
G=0;
for i=1:length(t)-1
    cla(handles.axes1)
    if i==1&~get(handles.checkhold,'Value')
       cla(handles.axes2)
       cla(handles.axes3)
    else
       if G==1
           set('XGrid','on');
       end
    end
    X=[xt(i)-L1,xt(i+1)-L1];
    Y=[yt(i)-L1-L2,yt(i+1)-L1-L2];
    T=[t(i),t(i+1)];
  % ---------------------------------------------------------------------  
    spr21=linspace(xt(i)+ms1/180,yt(i)-ms2/180,ks2);
    spr22=zeros(1,ks2);
    
    for m=1:floor(ks2/2)
        spr22(2*m)=minms/113;
    end
   % --------------------------------------------------------------------- 
    spr11=linspace(0,xt(i)-ms1/140,ks1);
    spr12=zeros(1,ks1);
    
    for m=1:floor(ks1/2)
        spr12(2*m)=ms1/113;
    end
    % ---------------------------------------------------------------------
    %%%%AXES 1%%%%
    axes(handles.axes1)
    spr11(1,1)=0;spr11(1,end)=xt(i);
    spr21(1,1)=xt(i);spr21(1,end)=yt(i);
    spr12(1,1)=ms1/114;spr12(1,end)=ms1/114;
    spr22(1,1)=minms/114;spr22(1,end)=minms/114;
    plot(spr21,spr22,'-','color',[0 .502 0])
    plot(spr11,spr12,'-','color',[1 .502 0])
    plot(xt(i),ms1/113,'sqr','markersize',ms1,'markerface','b','markeredgecolor','b')
    plot(yt(i),ms2/113,'sqr','markersize',ms2,'markerface','r','markeredgecolor','r')
    plot([-200*abs(mmin)-200 200*abs(mmax)+200],[-.5 -.5],'k-','linewidth',46)
    plot([0 0],[-1 3],'k-','linewidth',3)
    %%%%Axes 2
    axes(handles.axes2);
    r2(i)=plot(T,X,'.-','markerface','b','color','b');
    set(r2(i),'uicontextmenu',handles.cmenu2);
    
    %%%%Axes 3
    axes(handles.axes3);
    r3(i)=plot(T,Y,'.-','markerface','r','color','r');
    set(r3(i),'uicontextmenu',handles.cmenu3);
    drawnow;
end
axes(handles.axes2);
legend('x')
axes(handles.axes3);
legend('y')
handles.r2=r2;
handles.r3=r3;
guidata(hObject, handles);
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
function cmenu2_x_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes2,'Type','line');
ylim=get(handles.axes2,'ylim');
ymax=max(max(handles.xt,ylim(2)));
ymin=min(min(handles.xt,ylim(1)));
set(handles.axes2,'ylim',[ymin ymax]);
if ~get(handles.checkhold,'Value')
    cla(handles.axes2)
    r2=plot(handles.t,handles.xt,'.-','LineWidth',1,'Color','b');
else
    r2=plot(handles.t,handles.xt,'.-','LineWidth',1,'Color','b');
end
set(r2,'uicontextmenu',handles.cmenu2);
legend(r2,'x')

% --------------------------------------------------------------------
function cmenu2_v_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2_v (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h = findobj(handles.axes2,'Type','line');
ylim=get(handles.axes2,'ylim');
ymax=max(max(handles.vxt,ylim(2)));
ymin=min(min(handles.vxt,ylim(1)));
set(handles.axes2,'ylim',[ymin ymax]);
if ~get(handles.checkhold,'Value')
    cla(handles.axes2);
    r2=plot(handles.t,handles.vxt,'o-','LineWidth',1,'Color','m');
else
    r2=plot(handles.t,handles.vxt,'o-','LineWidth',1,'Color','m');
end
set(r2,'uicontextmenu',handles.cmenu2);
legend(r2,'v')
% --------------------------------------------------------------------
function cmenu2_a_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes2,'Type','line');
ylim=get(handles.axes2,'ylim');
ymax=max(max(handles.axt,ylim(2)));
ymin=min(min(handles.axt,ylim(1)));
set(handles.axes2,'ylim',[ymin ymax]);
if ~get(handles.checkhold,'Value')
    cla(handles.axes2)
    r2=plot(handles.t,handles.axt,'*-','LineWidth',1,'Color','k');
else
    r2=plot(handles.t,handles.axt,'*-','LineWidth',1,'Color','k');
end
set(r2,'uicontextmenu',handles.cmenu2);
legend(r2,'a')


% --------------------------------------------------------------------
function cmenu2_red_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2_red (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes2,'Type','line');
set(h,'Color','r');

% --------------------------------------------------------------------
function cmenu2_green_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2_green (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h = findobj(handles.axes2,'Type','line');
set(h,'Color','g');
% --------------------------------------------------------------------
function cmenu2_blue_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2_blue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes2,'Type','line');
set(h,'Color','b');

% --------------------------------------------------------------------
function cmenu3_y_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes3,'Type','line');
ylim=get(handles.axes3,'ylim');
ymax=max(max(handles.yt,ylim(2)));
ymin=min(min(handles.yt,ylim(1)));
set(handles.axes3,'ylim',[ymin ymax]);
if ~get(handles.checkhold,'Value')
    cla(handles.axes3);
    r3=plot(handles.t,handles.yt,'.-','LineWidth',1,'Color','r');
else
    r3=plot(handles.t,handles.yt,'.-','LineWidth',1,'Color','r');
end
set(r3,'uicontextmenu',handles.cmenu3);
legend(r3,'y')

% --------------------------------------------------------------------
function cmenu3_v_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3_v (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes3,'Type','line');
ylim=get(handles.axes3,'ylim');
min(handles.vyt);
ymax=max(max(handles.vyt,ylim(2)));
ymin=min(min(handles.vyt,ylim(1)));
set(handles.axes3,'ylim',[ymin ymax]);
if ~get(handles.checkhold,'Value')
    cla(handles.axes3);
    r3=plot(handles.t,handles.vyt,'o-','LineWidth',1,'color','m');
else
    r3=plot(handles.t,handles.vyt,'o-','LineWidth',1,'color','m');
end
set(r3,'uicontextmenu',handles.cmenu3);
legend(r3,'v')

% --------------------------------------------------------------------
function cmenu3_a_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes3,'Type','line');
ylim=get(handles.axes3,'ylim');
ymax=max(max(handles.ayt,ylim(2)));
ymin=min(min(handles.ayt,ylim(1)));
set(handles.axes3,'ylim',[ymin ymax]);
if ~get(handles.checkhold,'Value')
    cla(handles.axes3);
    r3=plot(handles.t,handles.ayt,'*-','LineWidth',1,'Color','k');
else
    r3=plot(handles.t,handles.ayt,'*-','LineWidth',1,'Color','k');
end
set(r3,'uicontextmenu',handles.cmenu3);
legend(r3,'a')

% --------------------------------------------------------------------
function cmenu3_red_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3_red (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes3,'Type','line');
set(h,'Color','r');

% --------------------------------------------------------------------
function cmenu3_green_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3_green (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes3,'Type','line');
set(h,'Color','g');

% --------------------------------------------------------------------
function cmenu3_blue_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3_blue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes3,'Type','line');
set(h,'Color','b');

% --------------------------------------------------------------------
function menu_grid_Callback(hObject, eventdata, handles)
% hObject    handle to menu_grid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if  strcmp(get(handles.axes1,'XGrid'),'off')    
    %w=1;
    set(handles.axes1,'XGrid','on')
    set(handles.axes1,'YGrid','on')
    set(handles.axes2,'XGrid','on')
    set(handles.axes2,'YGrid','on')
    set(handles.axes3,'XGrid','on')
    set(handles.axes3,'YGrid','on')
    set(handles.menu_grid,'Checked','on')
    set(handles.checkgrid,'Value',1)
else
    %w=0;
    set(handles.axes1,'XGrid','off')
    set(handles.axes1,'YGrid','off')
    set(handles.axes2,'XGrid','off')
    set(handles.axes2,'YGrid','off')
    set(handles.axes3,'XGrid','off')
    set(handles.axes3,'YGrid','off')
    set(handles.menu_grid,'Checked','off')
    set(handles.checkgrid,'Value',0)
end

% --------------------------------------------------------------------
function menu_hold_Callback(hObject, eventdata, handles)
% hObject    handle to menu_hold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if  get(handles.checkhold,'Value')==0    
    set(handles.menu_hold,'Checked','on')
    set(handles.checkhold,'Value',1)
else
    set(handles.menu_hold,'Checked','off')
    set(handles.checkhold,'Value',0)
end
% --------------------------------------------------------------------
function menu_tspan_Callback(hObject, eventdata, handles)
% hObject    handle to menu_tspan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt={'Enter Time Span ( [T0 T] or linspace(T0,T,N) )'};
name='Time Span';
numlines=1;
danswer={handles.tspan};
answer=inputdlg(prompt,name,numlines,danswer);
[i j]=size(answer);
if i==0
    answer{1}=handles.tspan;
end
handles.tspan=answer{1};
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_l_Callback(hObject, eventdata, handles)
% hObject    handle to menu_l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt={'L1','L2'};
name='L';
numlines=1;
danswer={handles.L1,handles.L2};
answer=inputdlg(prompt,name,numlines,danswer);
[i j]=size(answer);
if i==0
    answer{1}=handles.L1;
    answer{2}=handles.L2;
end
handles.L1=answer{1};
handles.L2=answer{2};
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_errortol_Callback(hObject, eventdata, handles)
% hObject    handle to menu_errortol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt={'Real Tol','Absolute Tol'};
name='Error Tolerance';
numlines=1;
danswer={handles.rt,handles.at};
answer=inputdlg(prompt,name,numlines,danswer);
[i j]=size(answer);
if i==0
    answer{1}=handles.rt;
    answer{2}=handles.at;
end
handles.rt=answer{1};
handles.at=answer{2};
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_about_Callback(hObject, eventdata, handles)
% hObject    handle to menu_about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
helpdlg('Spring Project                                                          Ali Soltani    (soltani.110@gmail.com)','About') 

% --------------------------------------------------------------------
function cmenu2_plot_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function cmenu2_color_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2_color (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function cmenu3_plot_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function cmenu3_color_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3_color (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menu_axes_Callback(hObject, eventdata, handles)
% hObject    handle to menu_axes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menu_other_Callback(hObject, eventdata, handles)
% hObject    handle to menu_other (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menu_help_Callback(hObject, eventdata, handles)
% hObject    handle to menu_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function cmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function cmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



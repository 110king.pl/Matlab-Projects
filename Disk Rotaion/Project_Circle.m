function varargout = Project_Circle(varargin)
% PROJECT_CIRCLE M-file for Project_Circle.fig
%      PROJECT_CIRCLE, by itself, creates a new PROJECT_CIRCLE or raises the existing
%      singleton*.
%
%      H = PROJECT_CIRCLE returns the handle to a new PROJECT_CIRCLE or the handle to
%      the existing singleton*.
%
%      PROJECT_CIRCLE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROJECT_CIRCLE.M with the given input arguments.
%
%      PROJECT_CIRCLE('Property','Value',...) creates a new PROJECT_CIRCLE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Project_Circle_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Project_Circle_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help Project_Circle

% Last Modified by GUIDE v2.5 22-Sep-2006 06:41:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Project_Circle_OpeningFcn, ...
                   'gui_OutputFcn',  @Project_Circle_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Project_Circle is made visible.
function Project_Circle_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Project_Circle (see VARARGIN)
clc;
disp('Pure Disk Rotation')
disp('Ali Soltani    (soltani.110@gmail.com)')
disp('<<< Summer 2006 >>>')
axes(handles.axes3)
text(0,.93,'    R :','fontsize',9,'FontWeight','bold')
text(0,.78,'    r :','fontsize',9,'FontWeight','bold')
text(0,.55,'\theta(0) :','fontsize',10,'FontWeight','bold')
text(0,.38,'\omega(0) :','fontsize',10,'FontWeight','bold')
text(0,.15,'    m :','fontsize',9,'FontWeight','bold')

% Choose default command line output for Project_Circle
handles.output = hObject;

% Update handles structure
set(handles.pause,'Visible','off')
set(handles.editth0,'String','pi/3')
set(handles.editrb,'String','5')
set(handles.editw0,'String','1')
set(handles.editrs,'String','3')
set(handles.editm,'String','1')
set(handles.axes1,'XTickLabel',{})
set(handles.axes1,'YTickLabel',{})
set(handles.axes2,'XTickLabel',{})
set(handles.axes2,'YTickLabel',{})
set(get(handles.axes2,'XLabel'),'String','Time(secends)');
set(get(handles.axes2,'YLabel'),'String','Energy(j)');
%set(handles.axes1,'YColor',[.735 .735 .735])
%set(handles.axes1,'XColor',[.735 .735 .735])
set(handles.axes1,'Color',[.735 .735 .735])
set(handles.axes1,'Box','on')
handles.tspan=(['[0 6]']);
handles.at='1e-6';
handles.rt='1e-3';
handles.g='9.8';
guidata(hObject, handles);

% UIWAIT makes Project_Circle wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Project_Circle_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editrb_Callback(hObject, eventdata, handles)
% hObject    handle to editrb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editrb as text
%        str2double(get(hObject,'String')) returns contents of editrb as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
    set(hObject,'BackgroundColor','r')
    uiwait;
    set(hObject,'String','')
    set(hObject,'BackgroundColor','w')
end

% --- Executes during object creation, after setting all properties.
function editrb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editrb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editrs_Callback(hObject, eventdata, handles)
% hObject    handle to editrs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editrs as text
%        str2double(get(hObject,'String')) returns contents of editrs as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
    set(hObject,'BackgroundColor','r')
    uiwait;
    set(hObject,'String','')
    set(hObject,'BackgroundColor','w')
end

% --- Executes during object creation, after setting all properties.
function editrs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editrs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editth0_Callback(hObject, eventdata, handles)
% hObject    handle to editth0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editth0 as text
%        str2double(get(hObject,'String')) returns contents of editth0 as a double


% --- Executes during object creation, after setting all properties.
function editth0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editth0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editw0_Callback(hObject, eventdata, handles)
% hObject    handle to editw0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editw0 as text
%        str2double(get(hObject,'String')) returns contents of editw0 as a double


% --- Executes during object creation, after setting all properties.
function editw0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editw0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function editm_Callback(hObject, eventdata, handles)
% hObject    handle to editm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editm as text
%        str2double(get(hObject,'String')) returns contents of editm as a double
user_input=str2double(get(hObject,'String'));
if isnan(user_input)
    errordlg('You must enter a number','Bad Input','model')
    set(hObject,'BackgroundColor','r')
    uiwait;
    set(hObject,'String','')
    set(hObject,'BackgroundColor','w')
end

% --- Executes during object creation, after setting all properties.
function editm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on button press in Start.
function Start_Callback(hObject, eventdata, handles)
% hObject    handle to Start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cstart=clock;
% --------------------------------------------------------------------- 
if  ~get(handles.checkp,'Value')
    cla(handles.axes4)
    set(handles.axes4,'Visible','off')
else
    cla(handles.axes4)
end
% ---------------------------------------------------------------------
th0=eval(get(handles.editth0,'String'));
R=str2double(get(handles.editrb,'String'));
w0=str2double(get(handles.editw0,'String'));
r=str2double(get(handles.editrs,'String'));
m=str2double(get(handles.editm,'String'));
RT=str2double(handles.rt);
AT=str2double(handles.at);
g=str2double(handles.g);
% ---------------------------------------------------------------------
tspan=str2num(handles.tspan);

% ---------------------------------------------------------------------
c1=m*g*(R-r);
c2=(3/2)*(m)*(r^2);
options=odeset('reltol',RT,'abstol',AT);
dy=@(t,y)[y(2)
          ((-r^2)*c1*sin(y(1)))/(c2*(R-r)^2)];
% ---------------------------------------------------------------------

% ---------------------------------------------------------------------
[t,th]=ode45(dy,tspan,[th0 w0],options);

% ---------------------------------------------------------------------
cend=clock;
es=cend-cstart;
total=es(5)*60+es(6);
disp({'Caomputing Taks',total,'secends'})
% ---------------------------------------------------------------------
U=m*g*(R-r)*(1-cos(th(:,1)));
T=3/4*m*r^2*(R/r*th(:,2)-th(:,2)).^2;
% ---------------------------------------------------------------------
handles.t=t;
handles.th=th(:,1);
handles.w=th(:,2);
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
%     Suggested Worksheet for use as M-file template to Create Animations.
% This template is designed to help students create Animated Mechanisms.
%
% Arash Mohtat
% Dynamics of Machinery
% Mech. Eng. Dep. of Tehran University   November-2005  (Azar 1384)
z=2*r/3;


%----------Initialize the figure for use with this Animation---------------
%%%%%%%% AXES 2 %%%%%%%%%%%%%
axes(handles.axes2);
cla
set(handles.axes2,'XTickLabelMode','auto')
set(handles.axes2,'YTickLabelMode','auto')
if ~(min(min(U),min(T))==max(U+T))
axis([min(tspan) max(tspan) min(min(U),min(T)) max(U+T)]);
else 
    xlim([min(tspan) max(tspan)]);
end
set(get(gca,'XLabel'),'String','Time(secends)');
set(get(gca,'YLabel'),'String','Energy(j)');
hold on
%%%%%%%% AXES 1 %%%%%%%%%%%%%
axes(handles.axes1);
cla
xmin=-R-z ;
xmax=R+z ;
ymin=-R-2*z ;
ymax=r;
if ~(ymin==ymax)
axis([xmin,xmax,ymin,ymax]);
end
axis off;
daspect([1 1 1]);hold on;
%--------------------------------------------------------------------------
x0=(R-r)*sin(th0);
y0=(r-R)*cos(th0);
%-----------Draw the Mechanism Components at their initial position--------
% Do the required pre-calculations
psi=th(:,1)*R/r;
[x1,y1]=circle(r,x0,y0);
[xf,yf]=circle2(R,0,0);
q1=[-R-z,-R-z,R+z,R+z];
q2=[-R-2*z,0,0,-R-2*z];
% ---------------------------------------------------------------------
xc=(R-r)*sin(th(1,1));
yc=(r-R)*cos(th(1,1));
xp=xc-r*sin(psi(1)-th(1,1));
yp=yc-r*cos(psi(1)-th(1,1));
% ---------------------------------------------------------------------
% Create appropriate Graphic Objects & Handles
reg=patch(q1,q2,'k');
circb=patch(xf,yf,'w','EdgeColor',[0.831 0.816 0.784],'FaceColor',[0.831 0.816 0.784]);
circs=patch(x1,y1,'y');
radius=plot([xc xp],[yc yp],'k-');

%--------------------------------------------------------------------------
x1=(R-r)*sin(th(:,1));
y1=(r-R)*cos(th(:,1));
%%%%%%%% AXES 4 %%%%%%%%%%%%%
if  get(handles.checkp,'Value')  
    axes(handles.axes4)
    plot(xf,yf,'k','linewidth',2);
    ylim([min(yf) max(yf)+3*r/2])
    xlim([min(xf) max(xf)])
    daspect([1 1 1])
    hold on
end
%set(handles.axes1,'NextPlot','add')
set(handles.axes1,'NextPlot','replace')
set(handles.axes1,'DrawMode','fast')
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%   Animation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for  i=2:size(t) 
     
    drawnow;  
    
    xc=(R-r)*sin(th(i,1));
    yc=(r-R)*cos(th(i,1));
    [x1,y1]=circle(r,xc,yc);
    xp=xc-r*sin(psi(i)-th(i,1));
    yp=yc-r*cos(psi(i)-th(i,1));
    
    %%%%%%%% AXES 1 %%%%%%%%%%%%%
    set(circs,'xdata',x1,'ydata',y1)
    set(radius,'xdata',[xc xp],'ydata',[yc yp])
    
    Px(i)=xp;
    Py(i)=yp;
    %%%%%%%% AXES 4 %%%%%%%%%%%%%
    if  get(handles.checkp,'Value') 
    axes(handles.axes4)
    plot(Px(i),Py(i),'.y')
    end
    %%%%%%%% AXES 2 %%%%%%%%%%%%%
    axes(handles.axes2)
    q1=plot([t(i-1) t(i)],[U(i-1) U(i)],'r.-','markersize',5);
    q2=plot([t(i-1) t(i)],[T(i-1) T(i)],'b.-','markersize',5);
    q3=plot([t(i-1) t(i)],[U(i-1)+T(i-1) U(i)+T(i)],'k.-','markersize',5);
end
set(handles.axes2,'uicontextmenu',handles.cmenu);
set(q1,'uicontextmenu',handles.cmenu);
set(q2,'uicontextmenu',handles.cmenu);
set(q3,'uicontextmenu',handles.cmenu);

handles.l=legend('U','T','U+T');
set(handles.l,'NextPlot','replace')
set(handles.l,'DrawMode','normal')
set(handles.l,'Position',[.44 .03 .1 .1])
guidata(hObject,handles)
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% ---------------------------------------------------------------------
% --- Executes on button press in pause.
function pause_Callback(hObject, eventdata, handles)
% hObject    handle to pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkp.
function checkp_Callback(hObject, eventdata, handles)
% hObject    handle to checkp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkp


% --- Executes on button press in checkhold.
function checkhold_Callback(hObject, eventdata, handles)
% hObject    handle to checkhold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkhold


% --- Executes on button press in checkgrid.
function checkgrid_Callback(hObject, eventdata, handles)
% hObject    handle to checkgrid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if  get(hObject,'Value')    
    set(handles.axes2,'XGrid','on')
    set(handles.axes2,'YGrid','on')
    set(handles.axes4,'XGrid','on')
    set(handles.axes4,'YGrid','on')
else
    set(handles.axes2,'XGrid','off')
    set(handles.axes2,'YGrid','off')
    set(handles.axes4,'XGrid','off')
    set(handles.axes4,'YGrid','off')
end
% Hint: get(hObject,'Value') returns toggle state of checkgrid


% --------------------------------------------------------------------
function cmenu_th_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu_th (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes2,'Type','line');
cla(handles.axes2)
ylim=get(handles.axes2,'ylim');
ymax=max(handles.th);
ymin=min(handles.th);
set(handles.axes2,'ylim',[ymin ymax]);
q2=plot(handles.t,handles.th,'o-m')
set(q2,'uicontextmenu',handles.cmenu);
legend(q2,'\theta')

% --------------------------------------------------------------------
function cmenu_w_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu_w (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = findobj(handles.axes2,'Type','line');
cla(handles.axes2)
ylim=get(handles.axes2,'ylim');
ymax=max(handles.w);
ymin=min(handles.w);
set(handles.axes2,'ylim',[ymin ymax]);
q2=plot(handles.t,handles.w,'o-k')
set(q2,'uicontextmenu',handles.cmenu);
legend(handles.l,'\omega')

% --------------------------------------------------------------------
function cmenu_p_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu_p (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cla(handles.axes2)
delete(legend)
% --------------------------------------------------------------------
function menu_error_Callback(hObject, eventdata, handles)
% hObject    handle to menu_error (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt={'Real Tol','Absolute Tol'};
name='Error Tolerance';
numlines=1;
danswer={handles.rt,handles.at};
answer=inputdlg(prompt,name,numlines,danswer);
[i j]=size(answer);
if i==0
    answer{1}=handles.rt;
    answer{2}=handles.at;
end
handles.rt=answer{1};
handles.at=answer{2};
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_about_Callback(hObject, eventdata, handles)
% hObject    handle to menu_about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
helpdlg('Circle Rotation Project                                                   Ali Soltani    (soltani.110@gmail.com)','About') 

% --------------------------------------------------------------------
function cmenu_plot_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menu_options_Callback(hObject, eventdata, handles)
% hObject    handle to menu_options (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menu_help_Callback(hObject, eventdata, handles)
% hObject    handle to menu_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function cmenu_Callback(hObject, eventdata, handles)
% hObject    handle to cmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --------------------------------------------------------------------
function menu_tspan_Callback(hObject, eventdata, handles)
% hObject    handle to menu_tspan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt={'Enter Time Span ( [T0 T] or linspace(T0,T,N) )'};
name='Time Span';
numlines=1;
danswer={handles.tspan};
answer=inputdlg(prompt,name,numlines,danswer);
[i j]=size(answer);
if i==0
    answer{1}=handles.tspan;
end
handles.tspan=answer{1};
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_g_Callback(hObject, eventdata, handles)
% hObject    handle to menu_g (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt={'Enter Gravity)'};
name='Gravity';
numlines=1;
danswer={handles.g};
answer=inputdlg(prompt,name,numlines,danswer);
[i j]=size(answer);
if i==0
    answer{1}=handles.g;
end
handles.g=answer{1};
guidata(hObject, handles);

% ---------------------------------------------------------------------
function [x,y]=circle(R,x0,y0)

th=linspace(0,2*pi);
x=R*cos(th)+x0;
y=R*sin(th)+y0;

% ---------------------------------------------------------------------
function [x,y]=circle2(R,x0,y0)

th=linspace(pi,2*pi);
x=R*cos(th)+x0;
y=R*sin(th)+y0;
% ---------------------------------------------------------------------











